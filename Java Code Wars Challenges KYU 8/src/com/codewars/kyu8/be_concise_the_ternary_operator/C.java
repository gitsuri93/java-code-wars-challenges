package com.codewars.kyu8.be_concise_the_ternary_operator;

/**
 * @author Surendra Reddy
 * 
 * Code has a restriction of only 137 characters. So had to truncate it this way
 */
class C {static String describeAge(int g) {return "You're a(n) "+(g<=12?"kid":g>=13&&g<=17?"teenager":g>=18&&g<=64?"adult":"elderly");}}