package com.codewars.kyu8.third_angle_of_a_triangle;

/**
 * @author Surendra Reddy
 */
public class ThirdAngle {

	public static int otherAngle(int angle1, int angle2) {
        return 180 - angle1 - angle2;
    }
}