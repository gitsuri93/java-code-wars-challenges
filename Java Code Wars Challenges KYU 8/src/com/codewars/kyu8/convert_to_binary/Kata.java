package com.codewars.kyu8.convert_to_binary;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static int toBinary(int n) {
		return Integer.parseInt(Integer.toBinaryString(n));
	}
}