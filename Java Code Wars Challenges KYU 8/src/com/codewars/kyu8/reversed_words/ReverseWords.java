package com.codewars.kyu8.reversed_words;

/**
 * @author Surendra Reddy
 */
public class ReverseWords {

	public static String reverseWords(String str){
		String res = "";
		String[] strArr = str.split(" ");

		for (int i = strArr.length - 1; i >= 0; i--) {
			res += strArr[i] + " ";
		}

		return res.trim();
	}
}