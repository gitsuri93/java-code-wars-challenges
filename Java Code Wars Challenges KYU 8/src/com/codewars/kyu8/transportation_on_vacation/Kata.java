package com.codewars.kyu8.transportation_on_vacation;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static int rentalCarCost(int d) {
		return (d * 40) - (d >= 7 ? 50 : (d >= 3 ? 20 : 0));
	}
}