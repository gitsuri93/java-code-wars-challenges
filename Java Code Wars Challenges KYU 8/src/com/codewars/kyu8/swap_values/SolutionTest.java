package com.codewars.kyu8.swap_values;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SolutionTest {

	@Test
	public void testSomething() {
		Integer[] args = new Integer[]{1,2}; 

		Swapper r = new Swapper(args);
		r.swapValues();
		assertEquals("Failed swapping numbers",2,r.arguments[0] );
		assertEquals("Failed swapping numbers",1,r.arguments[1] );
	}
}