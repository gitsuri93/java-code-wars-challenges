package com.codewars.kyu8.make_upper_case;

/**
 * @author Surendra Reddy
 */
public class Upper {

	public static String MakeUpperCase(String str){
		return str.toUpperCase();
	}
}