package com.codewars.kyu8.correct_mistakes_of_character_recognition_software;

/**
 * @author Surendra Reddy
 */
public class Correct {

	public static String correct(String string) {
		return string.replaceAll("5", "S").replaceAll("0", "O").replaceAll("1", "I");
	}
}