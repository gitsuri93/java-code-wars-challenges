package com.codewars.kyu8.opposite_number;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class OppositeExampleTests {

	@Test
	public void tests() {
		assertEquals(-1, Kata.opposite(1));
	}
}