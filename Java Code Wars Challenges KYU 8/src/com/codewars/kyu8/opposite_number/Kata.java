package com.codewars.kyu8.opposite_number;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static int opposite(int number) {
		return number * -1;
	}
}