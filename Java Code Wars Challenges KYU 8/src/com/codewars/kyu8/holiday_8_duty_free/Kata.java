package com.codewars.kyu8.holiday_8_duty_free;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static int dutyFree(int normPrice, int discount, int hol) {
		return (hol * 100) / (normPrice * discount);
	}
}