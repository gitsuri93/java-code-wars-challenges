package com.codewars.kyu8.remove_first_and_last_characters;

/**
 * @author Surendra Reddy
 */
public class RemoveChars {

	public static String remove(String str) {
		String res = "";

		for (int i = 0; i < str.length(); i++) {
			if(i != 0 && i != str.length() - 1) {
				res += str.charAt(i);
			}
		}

		return res;
	}
}