package com.codewars.kyu8.multiply;

/**
 * @author Surendra Reddy
 */
public class Multiply {

	public static Double multiply(double a, double b) {
        return a * b;
    }
}