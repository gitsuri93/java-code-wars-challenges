package com.codewars.kyu8.return_negative;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static int makeNegative(final int x) {
		return x > 0 ? x * -1 : x;
	}
}