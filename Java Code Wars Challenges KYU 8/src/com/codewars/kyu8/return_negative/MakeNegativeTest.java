package com.codewars.kyu8.return_negative;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MakeNegativeTest {

	@Test
	public void test1() {
		assertEquals(-42, Kata.makeNegative(42));
		assertEquals(-1, Kata.makeNegative(1));
		assertEquals(-5, Kata.makeNegative(-5));
		assertEquals(0, Kata.makeNegative(0));
	}
}