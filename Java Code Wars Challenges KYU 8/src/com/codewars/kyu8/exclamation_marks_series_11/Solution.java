package com.codewars.kyu8.exclamation_marks_series_11;

/**
 * @author Surendra Reddy
 */
public class Solution {

	public static String replace(final String s) {
		return s.replaceAll("A", "!").replaceAll("a", "!").replaceAll("E", "!").replaceAll("e", "!").replaceAll("I", "!").replaceAll("i", "!").replaceAll("O", "!").replaceAll("o", "!").replaceAll("U", "!").replaceAll("u", "!");
	}
}