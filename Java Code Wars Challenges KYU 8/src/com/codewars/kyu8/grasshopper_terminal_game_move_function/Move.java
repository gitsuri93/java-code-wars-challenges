package com.codewars.kyu8.grasshopper_terminal_game_move_function;

/**
 * @author Surendra Reddy
 */
public class Move {

	public static int move(int position, int roll) {
		return position + 2 * roll;
	}
}