package com.codewars.kyu8.get_mean_of_an_array;

import java.util.Arrays;

/**
 * @author Surendra Reddy
 */
public class School {

	public static int getAverage(int[] marks) {
		return (int) Arrays.stream(marks).average().getAsDouble();
	}
}