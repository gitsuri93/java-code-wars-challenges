package com.codewars.kyu8.first_non_consecutive_number;

/**
 * @author Surendra Reddy
 */
class FirstNonConsecutive {

	static Integer find(final int[] array) {
		if(array != null && array.length > 1) {
			for (int i = 1; i < array.length; i++) {
				if((array[i - 1] + 1) != array[i]) {
					return array[i];
				}
			}
		}

		return null;
	}
}