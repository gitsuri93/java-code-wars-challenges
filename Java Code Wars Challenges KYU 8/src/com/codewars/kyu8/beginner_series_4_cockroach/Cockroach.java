package com.codewars.kyu8.beginner_series_4_cockroach;

/**
 * @author Surendra Reddy
 */
public class Cockroach {

	public int cockroachSpeed(double x){
		return (int) Math.floor((x * 250) / 9);
	}
}