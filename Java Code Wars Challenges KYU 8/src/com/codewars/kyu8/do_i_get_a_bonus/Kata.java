package com.codewars.kyu8.do_i_get_a_bonus;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static String bonusTime(final int salary, final boolean bonus) {
		if(bonus) {
			return "\u00A3" + (10 * salary);
		} else {
			return "\u00A3" + salary;
		}
	}
}