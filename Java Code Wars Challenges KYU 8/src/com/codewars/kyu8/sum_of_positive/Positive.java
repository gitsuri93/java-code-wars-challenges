package com.codewars.kyu8.sum_of_positive;

/**
 * @author Surendra Reddy
 */
public class Positive {

	public static int sum(int[] arr) {
		return java.util.Arrays.stream(arr).filter(x -> x >= 0).sum();
	}
}