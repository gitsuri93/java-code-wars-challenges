package com.codewars.kyu8.function_3_multiplying_two_numbers;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static int multiply(int num1, int num2) {
		return num1 * num2;
	} 
}