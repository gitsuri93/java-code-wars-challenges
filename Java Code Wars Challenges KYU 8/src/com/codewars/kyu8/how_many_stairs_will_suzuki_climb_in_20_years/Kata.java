package com.codewars.kyu8.how_many_stairs_will_suzuki_climb_in_20_years;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static long stairsIn20(int[][] stairs) {
		long sum = 0;

		for (int i = 0; i < stairs.length; i++) {
			for (int j = 0; j < stairs[i].length; j++) {
				sum += stairs[i][j];
			}
		}

		return sum * 20;
	}
}