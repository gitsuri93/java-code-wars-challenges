package com.codewars.kyu8.function_1_hello_world;

/**
 * @author Surendra Reddy
 */
public class HelloWorld {

	public static String greet() {
		return "hello world!";
	}
}