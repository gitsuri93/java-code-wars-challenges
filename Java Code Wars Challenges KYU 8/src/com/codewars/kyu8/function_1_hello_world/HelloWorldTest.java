package com.codewars.kyu8.function_1_hello_world;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class HelloWorldTest {

	@Test
	public void testHelloWorld() throws Exception {
		assertEquals("hello world!", HelloWorld.greet());
	}
}