package com.codewars.kyu8.grasshopper_personalized_message;

/**
 * @author Surendra Reddy
 */
public class Kata {

	static String greet(String name, String owner) {
		if(name != null && owner != null && name.equals(owner)) {
			return "Hello boss";
		}

		return "Hello guest";
	}
}