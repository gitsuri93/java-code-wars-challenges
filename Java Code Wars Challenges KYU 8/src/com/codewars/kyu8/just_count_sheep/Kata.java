package com.codewars.kyu8.just_count_sheep;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static String countingSheep(int num) {
		String res = "";

		for (int i = 1; i <= num; i++) {
			res += (i + " sheep...");
		}

		return res;
	}
}