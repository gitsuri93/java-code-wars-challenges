package com.codewars.kyu8.merging_sorted_integer_arrays_without_duplicates;

import java.util.Arrays;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static int[] mergeArrays(int[] first, int[] second) {
		if(first.length > 0 || second.length > 0) {
			int[] finalArr = new int[first.length + second.length];

			System.arraycopy(first, 0, finalArr, 0, first.length);
			System.arraycopy(second, 0, finalArr, first.length, second.length);

			Arrays.sort(finalArr);

			return removeDuplicateElements(finalArr);
		}

		return new int[] {};
	}

	public static int[] removeDuplicateElements(int arr[]){  
		int[] temp = new int[arr.length];  
		int j = 0;

		for (int i = 0; i < arr.length - 1; i++){
			if (arr[i] != arr[i + 1]) {
				temp[j++] = arr[i];
			}
		}
		temp[j++] = arr[arr.length - 1];

		arr = new int[j];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = temp[i];
		}

		return arr;
	}
}