package com.codewars.kyu8.even_or_odd;

/**
 * @author Surendra Reddy
 */
public class EvenOrOdd {

	public static String even_or_odd(int number) {
        return (number % 2 == 0) ? "Even" : "Odd";
    }
}