package com.codewars.kyu8.counting_sheep;

/**
 * @author Surendra Reddy
 */
public class Counter {

	public int countSheeps(Boolean[] arrayOfSheeps) {
		int numberOfSheep = 0;

		for (Boolean sheep : arrayOfSheeps) {
			if(sheep != null && sheep.booleanValue()) {
				numberOfSheep++;
			}
		}

		return numberOfSheep;
	}
}