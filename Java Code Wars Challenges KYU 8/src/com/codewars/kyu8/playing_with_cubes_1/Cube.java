package com.codewars.kyu8.playing_with_cubes_1;

/**
 * @author Surendra Reddy
 */
public class Cube {

	private int side;

	public int getSide() {
		return side;
	}

	public void setSide(int side) {
		this.side = side;
	}
}