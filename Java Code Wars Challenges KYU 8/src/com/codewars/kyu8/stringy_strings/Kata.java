package com.codewars.kyu8.stringy_strings;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static String stringy(int size) {
		String res = "";

		for (int i = 1; i <= size; i++) {
			if(i % 2 == 1) {
				res += "1";
			} else {
				res += "0";
			}
		}

		return res;
	}
}