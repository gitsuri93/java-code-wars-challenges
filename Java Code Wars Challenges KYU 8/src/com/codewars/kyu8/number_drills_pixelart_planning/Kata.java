package com.codewars.kyu8.number_drills_pixelart_planning;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static boolean isDivisible(int wallLength, int pixelSize) {
		return wallLength % pixelSize == 0;
	}
}