package com.codewars.kyu8.get_character_from_ascii_value;

/**
 * @author Surendra Reddy
 */
public class Ascii {

	public static char getChar(int c) {
		return (char) c;
	}
}