package com.codewars.kyu8.remove_exclamation_marks;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SolutionTest {

	@Test
	public void testSimpleString1() {
		assertEquals("Hello World", Solution.removeExclamationMarks("Hello World!"));
	}
}