package com.codewars.kyu8.remove_exclamation_marks;

/**
 * @author Surendra Reddy
 */
public class Solution {

	static String removeExclamationMarks(String s) {
        return s.replaceAll("!", "");
    }
}