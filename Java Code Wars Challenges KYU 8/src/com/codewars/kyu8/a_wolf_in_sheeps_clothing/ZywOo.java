package com.codewars.kyu8.a_wolf_in_sheeps_clothing;

/**
 * @author Surendra Reddy
 */
public class ZywOo {

	public static String warnTheSheep(String[] array) {
		if(array[array.length - 1] == "wolf") {
			return "Pls go away and stop eating my sheep";
		} else {
			for (int i = 0; i < array.length ; i++) {
				if(array[i] == "wolf") {
					return "Oi! Sheep number " + (array.length - i - 1) + "! You are about to be eaten by a wolf!";
				}
			}
		}

		return "";
	}
}