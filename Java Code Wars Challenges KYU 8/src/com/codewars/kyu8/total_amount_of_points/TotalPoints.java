package com.codewars.kyu8.total_amount_of_points;

/**
 * @author Surendra Reddy
 */
public class TotalPoints {

	public static int points(String[] games) {
		int sum = 0;

		for (int i = 0; i < games.length; i++) {
			int ourTeamScore = Integer.parseInt(games[i].split(":")[0]);
			int opponentScore = Integer.parseInt(games[i].split(":")[1]);

			if(ourTeamScore > opponentScore) {
				sum += 3;
			} else if(ourTeamScore == opponentScore) {
				sum += 1;
			}
		}

		return sum;
	}
}