package com.codewars.kyu8.you_cant_code_under_pressure_1;

/**
 * @author Surendra Reddy
 */
public class Java {

	public static int doubleInteger(int i) {
		return 2 * i;
	}
}