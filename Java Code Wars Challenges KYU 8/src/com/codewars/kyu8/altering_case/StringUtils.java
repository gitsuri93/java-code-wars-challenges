package com.codewars.kyu8.altering_case;

/**
 * @author Surendra Reddy
 */
public class StringUtils {

	public static String toAlternativeString(String string) {
		String res = "";
		char[] arr = string.toCharArray();

		for (int i=0; i<arr.length; i++) { 
			if (arr[i]>='A' && arr[i]<='Z') { 
				res += (char) (arr[i] + 'a' - 'A'); 
			} else if (arr[i]>='a' && arr[i]<='z') {
				res += (char) (arr[i] + 'A' - 'a');
			} else {
				res += arr[i];
			}
		}

		return res;
	}
}