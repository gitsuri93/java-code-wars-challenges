package com.codewars.kyu8.basic_subclasses_adam_and_eve;

/**
 * @author Surendra Reddy
 */
public class God {

	public static Human[] create() {
		return new Human[] {new Man(), new Woman()};
	}
}

class Human {

}

class Man extends Human {

}

class Woman extends Human {

}