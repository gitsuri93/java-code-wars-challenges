package com.codewars.kyu8.basic_subclasses_adam_and_eve;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class GodTest {

	@Test
	public void makingAdam(){
		Human[] paradise = God.create();
		assertEquals("Adam are a man", true ,paradise[0] instanceof Man);
	}
}