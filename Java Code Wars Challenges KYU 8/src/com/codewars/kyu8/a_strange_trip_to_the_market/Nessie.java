package com.codewars.kyu8.a_strange_trip_to_the_market;

/**
 * @author Surendra Reddy
 */
public class Nessie {

	public static boolean isLockNessMonster(String s){
		if(s.contains("tree fiddy") || s.contains("3.50") || s.contains("three fifty")) {
			return true;
		}

		return false;
	}
}