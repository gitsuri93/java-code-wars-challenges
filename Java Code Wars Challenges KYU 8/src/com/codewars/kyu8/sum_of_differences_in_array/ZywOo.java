package com.codewars.kyu8.sum_of_differences_in_array;

import java.util.Arrays;

/**
 * @author Surendra Reddy
 */
public class ZywOo {

	public static int sumOfDifferences(int[] arr) {
		int sum = 0;

		if(arr != null && arr.length > 1) {
			arr = descendingOrder(arr);
			for (int i = 0; i < arr.length - 1; i++) {
				sum += (arr[i] - arr[i + 1]);
			}
		}

		return sum;
	}

	public static int[] descendingOrder(int[] arr) {
		int[] desArr = new int[arr.length];
		Arrays.sort(arr);

		for (int i = 0; i < arr.length; i++) {
			desArr[i] = arr[arr.length - i - 1];
		}

		return desArr;
	}
}