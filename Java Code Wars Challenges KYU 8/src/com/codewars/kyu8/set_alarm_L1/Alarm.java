package com.codewars.kyu8.set_alarm_L1;

/**
 * @author Surendra Reddy
 */
public class Alarm {

	public static boolean setAlarm(boolean employed, boolean vacation) {
		return employed && !vacation;
	}
}