package com.codewars.kyu8.reversed_sequence;

/**
 * @author Surendra Reddy
 */
public class Sequence {

	public static int[] reverse(int n){
		int[] res = new int[n];

		for (int i = 0; i < res.length; i++) {
			res[i] = n--;
		}

		return res;
	}
}