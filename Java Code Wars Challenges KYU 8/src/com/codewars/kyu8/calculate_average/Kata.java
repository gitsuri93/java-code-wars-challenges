package com.codewars.kyu8.calculate_average;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static double find_average(int[] array){
		int sum = 0;

		for (int i = 0; i < array.length; i++) {
			sum += array[i];
		}

		return (double)sum / array.length;
	}
}