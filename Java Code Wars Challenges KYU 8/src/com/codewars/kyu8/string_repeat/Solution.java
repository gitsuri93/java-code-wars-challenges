package com.codewars.kyu8.string_repeat;

/**
 * @author Surendra Reddy
 */
public class Solution {

	public static String repeatStr(final int repeat, final String string) {
		String res = "";

		for (int i = 1; i <= repeat; i++) {
			res += string;
		}

		return res;
	}
}