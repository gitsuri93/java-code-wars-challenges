package com.codewars.kyu8.easy_logs;

/**
 * @author Surendra Reddy
 */
public class EasyLogs {

	public static double logs(double x, double a, double b) {
		return (Math.log(a) + Math.log(b)) / Math.log(x);
	}
}