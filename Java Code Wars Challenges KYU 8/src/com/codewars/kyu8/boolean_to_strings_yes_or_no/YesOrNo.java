package com.codewars.kyu8.boolean_to_strings_yes_or_no;

/**
 * @author Surendra Reddy
 */
public class YesOrNo {

	public static String boolToWord(boolean b) {
		return b ? "Yes" : "No";
	}
}