package com.codewars.kyu8.grasshopper_summation;

/**
 * @author Surendra Reddy
 */
public class GrassHopper {

	public static int summation(int n) {
		return (n * (n + 1)) / 2;
	}
}