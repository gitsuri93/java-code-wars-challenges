package com.codewars.kyu8.simple_fun_1_seats_in_theater;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static int seatsInTheater(int nCols, int nRows, int col, int row) {
		return (nRows - row) * (nCols - col + 1);
	}
}