package com.codewars.kyu8.sleigh_authentication;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SolutionTest {

	@Test
	public void test_authentication() {
		assertEquals(true, Sleigh.authenticate("Santa Claus", "Ho Ho Ho!"));
		assertEquals(false, Sleigh.authenticate("Santa", "Ho Ho Ho!"));
		assertEquals(false, Sleigh.authenticate("Santa Claus", "Ho Ho Ho"));
	}
}