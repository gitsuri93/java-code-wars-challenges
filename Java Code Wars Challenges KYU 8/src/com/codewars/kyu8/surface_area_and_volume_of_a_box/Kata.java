package com.codewars.kyu8.surface_area_and_volume_of_a_box;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static int[] getSize(int w,int h,int d) {
		return new int[] {2 * (w * h + h * d + w * d), w * h * d};
	}
}