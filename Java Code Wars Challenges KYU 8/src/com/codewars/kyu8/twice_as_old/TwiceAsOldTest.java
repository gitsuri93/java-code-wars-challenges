package com.codewars.kyu8.twice_as_old;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TwiceAsOldTest {

	@Test
	public void testSomething() {
		assertEquals(30, TwiceAsOld.twiceAsOld(30,0));
		assertEquals(16, TwiceAsOld.twiceAsOld(30,7));
		assertEquals(15, TwiceAsOld.twiceAsOld(45,30));      
	}
}