package com.codewars.kyu8.twice_as_old;

/**
 * @author Surendra Reddy
 */
public class TwiceAsOld {

	public static int twiceAsOld(int dadYears, int sonYears){
		return Math.abs(dadYears - (2 * sonYears));
	}
}