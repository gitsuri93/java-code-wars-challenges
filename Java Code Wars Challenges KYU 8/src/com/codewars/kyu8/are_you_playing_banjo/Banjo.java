package com.codewars.kyu8.are_you_playing_banjo;

/**
 * @author Surendra Reddy
 */
public class Banjo {

	public static String areYouPlayingBanjo(String name) {
		return name + ((name.charAt(0) == 'R' || name.charAt(0) == 'r') ? " plays banjo" : " does not play banjo");
	}
}