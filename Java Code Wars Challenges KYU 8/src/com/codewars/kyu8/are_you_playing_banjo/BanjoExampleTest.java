package com.codewars.kyu8.are_you_playing_banjo;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BanjoExampleTest {

	@Test
	public void PeopleThatPlayBanjo() {
		assertEquals( "Nope!", "Martin does not play banjo", Banjo.areYouPlayingBanjo("Martin"));
		assertEquals( "Nope!", "Rikke plays banjo", Banjo.areYouPlayingBanjo("Rikke"));
	}
}