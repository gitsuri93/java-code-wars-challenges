package com.codewars.kyu8.keep_hydrated;

/**
 * @author Surendra Reddy
 */
public class KeepHydrated {

	public int Liters(double time)  {
		return (int) Math.floor(time * 0.5);
	}
}