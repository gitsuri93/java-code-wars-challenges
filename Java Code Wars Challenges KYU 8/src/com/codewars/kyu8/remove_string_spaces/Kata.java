package com.codewars.kyu8.remove_string_spaces;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static String noSpace(final String x) {
		return (x != null && !x.isEmpty()) ? x.replaceAll(" ", "") : x;
	}
}