package com.codewars.kyu8.is_it_a_digit;

/**
 * @author Surendra Reddy
 */
public class StringUtils {

	public static boolean isDigit(String s) {
		try {
			int sInt = Integer.parseInt(s);

			if(sInt >= 0 && sInt <= 9) {
				return true;
			}
		} catch (Exception e) {
			// Do Nothing
		}

		return false;
	}
}