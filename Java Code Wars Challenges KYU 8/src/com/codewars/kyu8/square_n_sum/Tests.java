package com.codewars.kyu8.square_n_sum;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class Tests {

	@Test
	public void testBasic() {
		assertEquals(9, Kata.squareSum(new int[] {1,2,2}));
		assertEquals(5, Kata.squareSum(new int[] {1,2}));
		assertEquals(50, Kata.squareSum(new int[] {5,-3,4}));
	}
}