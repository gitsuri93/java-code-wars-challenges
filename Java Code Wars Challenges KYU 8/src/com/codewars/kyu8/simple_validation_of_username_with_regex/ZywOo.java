package com.codewars.kyu8.simple_validation_of_username_with_regex;

/**
 * @author Surendra Reddy
 */
public class ZywOo {

	public static boolean validateUsr(String s) {
		if(s != null && !s.isEmpty() && s.length() >= 4 && s.length() <= 16) {
			return s.matches("[a-z0-9_]*");
		}

		return false;
	}
}