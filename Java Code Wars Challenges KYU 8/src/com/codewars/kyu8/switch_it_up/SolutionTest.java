package com.codewars.kyu8.switch_it_up;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SolutionTest {

	@Test
	public void basicTests() {
		assertEquals("One", Kata.switchItUp(1));
		assertEquals("Three", Kata.switchItUp(3));
		assertEquals("Five", Kata.switchItUp(5));
	}
}