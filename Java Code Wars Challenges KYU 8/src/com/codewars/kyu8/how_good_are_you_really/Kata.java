package com.codewars.kyu8.how_good_are_you_really;

import java.util.Arrays;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static boolean betterThanAverage(int[] classPoints, int yourPoints) {
		return yourPoints > ((Arrays.stream(classPoints).sum() + yourPoints) / (classPoints.length + 1)) ? true : false;
	}
}