package com.codewars.kyu8.count_of_positives_sum_of_negatives;

import java.util.Arrays;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static int[] countPositivesSumNegatives(int[] input) {
		return (input != null && input.length > 0) ? new int[] { (int) Arrays.stream(input).filter(x -> x > 0).count(), Arrays.stream(input).filter(x -> x < 0).sum()} : new int[] {};
	}
}