package com.codewars.kyu8.enumerable_magic_25;

/**
 * @author Surendra Reddy
 */
public class ZywOo {

	public static int[] take(int[] arr, int n) {
		if(arr != null && arr.length > 0) {
			if(n > arr.length) {
				n = arr.length;
			}

			int[] res = new int[n];
			for (int i = 0; i < n; i++) {
				res[i] = arr[i];
			}

			return res;
		}

		return arr;
	}
}