package com.codewars.kyu8.hex_to_decimal;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static int hexToDec(final String hexString) {
		return Integer.parseInt(hexString, 16);
	}
}