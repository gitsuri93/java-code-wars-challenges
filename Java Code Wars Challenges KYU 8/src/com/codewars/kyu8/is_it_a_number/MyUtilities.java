package com.codewars.kyu8.is_it_a_number;

/**
 * @author Surendra Reddy
 */
public class MyUtilities {

	public boolean isDigit(String s) {
		try {
			Double.parseDouble(s.trim());
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}