package com.codewars.kyu8.get_nth_even_number;

/**
 * @author Surendra Reddy
 */
public class Num {

	public static int nthEven(int n) {
		return 2 * (n - 1);
	}
}