package com.codewars.kyu8.convert_number_to_reversed_array_of_digits;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static int[] digitize(long n) {
		String str = Long.toString(n);

		int[] arr = new int[str.length()];

		for (int i = 0; i < arr.length; i++) {
			arr[arr.length - i - 1] = Integer.parseInt(str.substring(i, i + 1));
		}

		return arr;
	}
}