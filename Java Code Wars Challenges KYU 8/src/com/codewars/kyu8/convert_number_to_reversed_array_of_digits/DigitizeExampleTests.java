package com.codewars.kyu8.convert_number_to_reversed_array_of_digits;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

public class DigitizeExampleTests {

	@Test
	public void tests() {
		assertArrayEquals(new int[] {1, 3, 2, 5, 3}, Kata.digitize(35231));
	}
}