package com.codewars.kyu8.well_of_ideas_easy_version;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static String well(String[] x) {
		if(null != x && x.length > 0) {
			int numberOfGoodIdeas = 0;

			for (int i = 0; i < x.length; i++) {
				if(x[i].equals("good")) {
					numberOfGoodIdeas++;
				}
			}

			if(numberOfGoodIdeas > 2) {
				return "I smell a series!";
			} else if(numberOfGoodIdeas >= 1) {
				return "Publish!";
			}
		}

		return "Fail!";
	}
}