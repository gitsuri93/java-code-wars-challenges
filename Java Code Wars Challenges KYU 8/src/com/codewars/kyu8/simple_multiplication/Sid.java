package com.codewars.kyu8.simple_multiplication;

/**
 * @author Surendra Reddy
 */
public class Sid {

	public static int simpleMultiplication(int n) {
		return n % 2 == 0 ? n * 8 : n * 9;
	}
}