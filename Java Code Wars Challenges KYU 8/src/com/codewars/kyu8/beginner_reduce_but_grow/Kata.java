package com.codewars.kyu8.beginner_reduce_but_grow;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static int grow(int[] x){
		int res = 1;

		for (int i = 0; i < x.length; i++) {
			res *= x[i];
		}

		return res;
	}
}