package com.codewars.kyu8.invert_values;

import java.util.Arrays;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static int[] invert(int[] array) {
		return Arrays.stream(array).map(x -> x * -1).toArray();
	}
}