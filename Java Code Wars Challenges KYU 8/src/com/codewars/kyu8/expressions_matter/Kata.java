package com.codewars.kyu8.expressions_matter;

import java.util.Arrays;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static int expressionsMatter(int a, int b, int c) {
		int[] arr = new int[] {
				a + b + c,
				a * b * c,
				a * (b + c),
				(a * b) + c,
				a + (b * c),
				(a + b) * c
		};

		Arrays.sort(arr);
		return arr[arr.length - 1];
	}
}