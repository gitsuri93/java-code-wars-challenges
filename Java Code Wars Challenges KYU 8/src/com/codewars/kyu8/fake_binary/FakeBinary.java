package com.codewars.kyu8.fake_binary;

/**
 * @author Surendra Reddy
 */
public class FakeBinary {

	public static String fakeBin(String numberString) {
		String res = "";

		for (int i = 0; i < numberString.length(); i++) {
			int digit = Integer.parseInt(numberString.substring(i, i + 1));

			if(digit < 5) {
				res += "0";
			} else {
				res += "1";
			}
		}

		return res;
	}
}