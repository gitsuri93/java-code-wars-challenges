package com.codewars.kyu8.exclamation_marks_series_6;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static String remove(String s, int n){
		int count = 1;
		String res = "";

		for (int i = 0; i < s.length(); i++) {
			if(s.charAt(i) != '!' || count > n) {
				res += s.charAt(i);
			} else {
				count++;
			}
		}

		return res;
	}
}