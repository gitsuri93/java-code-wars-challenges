package com.codewars.kyu8.will_there_be_enough_space;

/**
 * @author Surendra Reddy
 */
public class Bob {

	public static int enough(int cap, int on, int wait){
		return (cap - on >= wait) ? 0 : wait - cap + on;
	}
}