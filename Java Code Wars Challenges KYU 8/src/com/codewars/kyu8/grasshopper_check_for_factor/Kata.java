package com.codewars.kyu8.grasshopper_check_for_factor;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static boolean checkForFactor(int base, int factor) {
		return base % factor == 0;
	}
}