package com.codewars.kyu8.convert_boolean_to_string;

/**
 * @author Surendra Reddy
 */
public class BooleanToString {

	public static String convert(boolean b){
		return String.valueOf(b);
	}
}