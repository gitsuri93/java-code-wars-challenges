package com.codewars.kyu8.exclusive_or_logical_operator;

/**
 * @author Surendra Reddy
 */
public class XOR {

	public static boolean xor(boolean a, boolean b) {
        return a && !b || !a && b;
    }
}