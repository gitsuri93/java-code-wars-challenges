package com.codewars.kyu8.my_head_is_at_the_wrong_end;

/**
 * @author Surendra Reddy
 */
public class WrongEndHead {

	public static String[] fixTheMeerkat(String[] arr) {
		String temp = arr[2];
		arr[2] = arr[0];
		arr[0] = temp;
		return arr;
	}
}