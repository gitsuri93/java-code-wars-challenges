package com.codewars.kyu8.sentence_smash;

/**
 * @author Surendra Reddy
 */
public class SmashWords {

	public static String smash(String... words) {
		String res = "";

		for (int i = 0; i < words.length; i++) {
			res += (words[i] + " ");
		}

		return res.trim();
	}
}