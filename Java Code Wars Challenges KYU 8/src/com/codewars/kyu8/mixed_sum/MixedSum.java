package com.codewars.kyu8.mixed_sum;

import java.util.List;

/**
 * @author Surendra Reddy
 *
 * Given an array of integers as strings and numbers, return the sum of the array values as if all were numbers.
 * Return your answer as a number.
 */
public class MixedSum {

	public int sum(List<?> mixed) {
		if(null != mixed && !mixed.isEmpty()) {
			int sum = 0;

			for (int i = 0; i < mixed.size(); i++) {
				try {
					if(mixed.get(i) instanceof String) {
						sum += Integer.parseInt((String)mixed.get(i));
					} else {
						sum += (Integer)mixed.get(i);
					}
				} catch (Exception e) {
					// TODO: handle exception
					continue;
				}
			}

			return sum;
		}

		return -1;
	}
}