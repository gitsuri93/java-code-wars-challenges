package com.codewars.kyu8.multiple_of_index;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Surendra Reddy
 */
public class ZywOo {

	public static int[] multipleOfIndex(int[] array) {
		List<Integer> list = new ArrayList<>();

		for (int i = 1; i < array.length; i++) {
			if(array[i] % i == 0) {
				list.add(array[i]);
			}
		}

		int[] res = new int[list.size()];
		for (int i = 0; i < res.length; i++) {
			res[i] = list.get(i);
		}

		return res;
	}
}