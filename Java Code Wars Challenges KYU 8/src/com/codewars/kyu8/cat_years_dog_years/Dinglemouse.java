package com.codewars.kyu8.cat_years_dog_years;

/**
 * @author Surendra Reddy
 */
public class Dinglemouse {

	public static int[] humanYearsCatYearsDogYears(final int humanYears) {
		return new int[]{humanYears, getCalculatedYears(humanYears, 4), getCalculatedYears(humanYears, 5)};
	}

	private static int getCalculatedYears(int humanYears, int x) {
		return humanYears == 1 ? 15 : (humanYears == 2 ? 24 : (24 + x * (humanYears - 2)));
	}
}