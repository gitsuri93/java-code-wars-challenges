package com.codewars.kyu8.subtract_the_sum;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SolutionTest {

	@Test
	public void basicTest() {
		assertEquals("apple", Kata.subtractSum(10));
		assertEquals("apple", Kata.subtractSum(325));
	}
}