package com.codewars.kyu8.sort_and_star;

import java.util.Arrays;

/**
 * @author Surendra Reddy
 */
public class SortAndStar {

	public static String twoSort(String[] s) {
		if(s != null && s.length > 0) {
			Arrays.sort(s);

			String res = "";
			for (int i = 0; i < s[0].length(); i++) {
				if(i == s[0].length() - 1) {
					res += s[0].charAt(i);
				} else {
					res += (s[0].charAt(i)  + "***");
				}
			}

			return res;
		}

		return null;
	}
}