package com.codewars.kyu8.convert_number_to_string;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static String numberToString(int num) {
		return Integer.toString(num);
	}
}