package com.codewars.kyu8.century_from_year;

/**
 * @author Surendra Reddy
 */
public class Solution {

	public static int century(int number) {
		return (number % 100 == 0) ? number / 100 : ((number - (number % 100)) / 100) + 1;
	}
}