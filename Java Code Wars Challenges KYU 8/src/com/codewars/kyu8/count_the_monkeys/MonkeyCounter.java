package com.codewars.kyu8.count_the_monkeys;

/**
 * @author Surendra Reddy
 */
public class MonkeyCounter {

	public static int[] monkeyCount(final int n){
		int[] arr = new int[n];

		for (int i = 0; i < arr.length; i++) {
			arr[i] = i + 1;
		}

		return arr;
	}
}