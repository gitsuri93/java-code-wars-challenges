package com.codewars.kyu8.volume_of_a_cuboid;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static double getVolumeOfCuboid(final double length, final double width, final double height) {
		return length * width * height;
	}
}