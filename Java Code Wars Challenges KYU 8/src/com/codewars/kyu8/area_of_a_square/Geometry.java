package com.codewars.kyu8.area_of_a_square;

/**
 * @author Surendra Reddy
 */
public class Geometry {

	public static double squareArea(double A){
		return Math.round(((4 * A * A) / (Math.PI * Math.PI)) * 100.0) / 100.0;
	}
}