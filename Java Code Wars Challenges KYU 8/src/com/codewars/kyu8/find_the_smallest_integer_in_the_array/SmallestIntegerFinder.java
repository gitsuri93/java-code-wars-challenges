package com.codewars.kyu8.find_the_smallest_integer_in_the_array;

import java.util.Arrays;

/**
 * @author Surendra Reddy
 */
public class SmallestIntegerFinder {

	public static int findSmallestInt(int[] args) {
		return Arrays.stream(args).min().getAsInt();
	}
}