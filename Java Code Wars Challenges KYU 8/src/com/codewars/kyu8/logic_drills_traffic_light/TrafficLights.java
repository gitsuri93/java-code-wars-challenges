package com.codewars.kyu8.logic_drills_traffic_light;

/**
 * @author Surendra Reddy
 */
public class TrafficLights {

	public static String updateLight(String current) {
		switch (current) {
		case "green":
			return "yellow";

		case "yellow":
			return "red";

		case "red":
			return "green";

		default:
			return "";
		}
	}
}