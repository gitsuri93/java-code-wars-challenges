package com.codewars.kyu8.convert_string_to_array;

/**
 * @author Surendra Reddy
 */
public class Solution {

	public static String[] stringToArray(String s) {
		if(s != null && !s.isEmpty()) {
			return s.split(" ");
		}

		return new String[] {};
	}
}