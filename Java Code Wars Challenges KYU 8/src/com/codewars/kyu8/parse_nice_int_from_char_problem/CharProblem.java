package com.codewars.kyu8.parse_nice_int_from_char_problem;

/**
 * @author Surendra Reddy
 */
public class CharProblem {

	public static int howOld(final String herOld) {
		return Integer.parseInt(herOld.substring(0, 1));
	}
}