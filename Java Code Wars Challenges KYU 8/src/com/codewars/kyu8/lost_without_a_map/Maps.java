package com.codewars.kyu8.lost_without_a_map;

/**
 * @author Surendra Reddy
 */
public class Maps {

	public static int[] map(int[] arr) {
		if(arr != null && arr.length > 0) {
			for (int i = 0; i < arr.length; i++) {
				arr[i] *= 2;
			}
		}

		return arr;
	}
}