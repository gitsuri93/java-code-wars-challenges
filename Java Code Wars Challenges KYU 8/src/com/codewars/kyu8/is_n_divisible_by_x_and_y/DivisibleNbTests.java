package com.codewars.kyu8.is_n_divisible_by_x_and_y;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class DivisibleNbTests {

	@Test
	public void test1() {
		assertEquals(true, DivisibleNb.isDivisible(12,4,3));
	}

	@Test
	public void test2() {
		assertEquals(false, DivisibleNb.isDivisible(3,3,4));
	}	
}