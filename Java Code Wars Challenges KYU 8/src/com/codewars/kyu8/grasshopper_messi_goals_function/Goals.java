package com.codewars.kyu8.grasshopper_messi_goals_function;

/**
 * @author Surendra Reddy
 */
public class Goals {

	public static int goals(int laLigaGoals, int copaDelReyGoals, int championsLeagueGoals) {
		return laLigaGoals + copaDelReyGoals + championsLeagueGoals;
	}
}