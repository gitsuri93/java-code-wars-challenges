package com.codewars.kyu8.count_odd_numbers_below_n;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static int oddCount(int n){
		return n % 2 == 1 ? (n - 1) / 2 : n / 2;
	}
}