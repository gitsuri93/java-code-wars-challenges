package com.codewars.kyu8.reversed_strings;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SolutionTest {

	@Test
	public void sampleTests() {
		assertEquals("dlrow", Kata.solution("world"));
	}
}