package com.codewars.kyu8.reversed_strings;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static String solution(String str) {
		String res = "";

		for (int i = str.length() - 1; i >= 0; i--) {
			res += str.charAt(i);
		}

		return res;
	}
}