package com.codewars.kyu8.dna_to_rna_conversion;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BioTest {

	@Test
	public void testDna() throws Exception {
		Bio b = new Bio();
		assertEquals("UUUU", b.dnaToRna("TTTT"));
	}

	@Test
	public void testDna2() throws Exception {
		Bio b = new Bio();
		assertEquals("GCAU", b.dnaToRna("GCAT"));
	}
}