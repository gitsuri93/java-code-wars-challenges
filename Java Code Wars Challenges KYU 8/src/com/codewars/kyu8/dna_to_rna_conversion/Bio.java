package com.codewars.kyu8.dna_to_rna_conversion;

/**
 * @author Surendra Reddy
 */
public class Bio {

	public String dnaToRna(String dna) {
        return dna.replaceAll("T", "U");
    } 
}