package com.codewars.kyu8.convert_string_to_number;

/**
 * @author Surendra Reddy
 */
public class StringToNumber {

	public static int stringToNumber(String str) {
		return Integer.parseInt(str);
	}
}