package com.codewars.kyu8.the_feast_of_many_beasts;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static boolean feast(String beast, String dish) {
		return beast.charAt(0) == dish.charAt(0) && beast.charAt(beast.length() - 1) == dish.charAt(dish.length() - 1);
	}
}