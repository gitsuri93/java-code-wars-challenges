package com.codewars.kyu8.determine_offspring_sex_based_on_genes;

/**
 * @author Surendra Reddy
 */
public class Kata {

	public static String chromosomeCheck(String sperm) {
		return "Congratulations! You're going to have a " + (sperm == "XX" ? "daughter." : "son.");
	}
}